package com.emp.manage;

import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @author employeeeee
 * @Descriotion: 新的配置类 之前的apater已经过时了
  * @date 2019/1/11 11:14
 */
@Configuration
public class WebMvcService extends WebMvcConfigurationSupport {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX+"/static/")
                .addResourceLocations("classpath:/META-INF/resources/")
                .addResourceLocations("file:D:/followers-videos/");
        super.addResourceHandlers(registry);
    }
}
