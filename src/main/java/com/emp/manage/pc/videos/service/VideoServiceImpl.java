package com.emp.manage.pc.videos.service;

import com.emp.manage.pc.videos.dao.VideoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/4 16:48
 */
@Service
public class VideoServiceImpl implements VideoService {

    @Resource
    private VideoMapper videoMapper;

    @Override
    public int selectCountVideos() {
        return videoMapper.selectCountVideos();
    }
}
