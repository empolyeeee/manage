package com.emp.manage.pc.users.service;

import com.emp.manage.config.utils.PagedResult;
import com.emp.manage.pc.users.domain.Users;
import com.emp.manage.pc.users.domain.UsersVo;

import java.util.List;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/4 10:29
 */

public interface UserService {

     PagedResult selectAllUsers(Integer page, Integer pageSize);

     /**
      * @author employeeeee
      * @Description:  查询用户数量
      * @date 2019/1/4 16:54
      * @params  * @param null
      */
     int selectCountUsers();


     /**
      * @author employeeeee
      * @Description: 点击列表 可以查看用户相关信息的详情
      * @date 2019/1/7 14:54
      * @params  * @param null
      */
     UsersVo queryUsersVoInfo(String userId);


     /**
      * @author employeeeee
      * @Description: 用户信息删除 可以多选删除多个用户信息
      * @date 2019/1/7 18:17
      * @params  * @param null
      */

     void deleteUsersList(Integer[] ids);
     
     
     /**
      * @author employeeeee
      * @Description: 根据用户ID值修改用户信息
      * @date 2019/1/11 13:56
      * @params  * @param users 返回用户信息
      */

     void updateUserInfo(Users users);


}
