package com.emp.manage.pc.users.service;

import com.emp.manage.config.utils.PagedResult;
import com.emp.manage.pc.users.dao.UsersCustomMapper;
import com.emp.manage.pc.users.dao.UsersMapper;
import com.emp.manage.pc.users.domain.Users;
import com.emp.manage.pc.users.domain.UsersVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/4 10:34
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UsersMapper usersMapper;

    @Resource
    private UsersCustomMapper usersCustomMapper;

    @Override
    public PagedResult selectAllUsers(Integer page,Integer pageSize) {
        PageHelper.startPage(page,pageSize);
        List<Users> list = usersMapper.selectAllUsers();

        PageInfo<Users> pageList = new PageInfo<>(list);
        PagedResult pagedResult = new PagedResult();
        pagedResult.setPage(page);
        pagedResult.setTotal(pageList.getPages());
        pagedResult.setRows(list);
        pagedResult.setRecords(pageList.getTotal());

        return pagedResult;
    }

    @Override
    public int selectCountUsers() {
        return usersMapper.selectCountUsers();
    }

    @Override
    public UsersVo queryUsersVoInfo(String userId) {
        Users users = usersMapper.selectByPrimaryKey(userId);
        UsersVo usersVo = usersCustomMapper.queryUsersVoInfo(userId);
        BeanUtils.copyProperties(users,usersVo);
        return usersVo;
    }

    @Override
    public void deleteUsersList(Integer[] ids) {
        usersCustomMapper.deleteUsersList(ids);
    }

    @Override
    public void updateUserInfo(Users users) {
        usersMapper.updateByPrimaryKeySelective(users);
    }
}
