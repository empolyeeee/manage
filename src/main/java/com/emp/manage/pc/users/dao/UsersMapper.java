package com.emp.manage.pc.users.dao;

import com.emp.manage.pc.users.domain.Users;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper

@Component
public interface UsersMapper {

    List<Users> selectAllUsers();

    int deleteByPrimaryKey(String id);

    int insert(Users record);

    int insertSelective(Users record);

    Users selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Users record);

    int updateByPrimaryKey(Users record);

    /**
     * @author employeeeee
     * @Description: 查询用户数量
     * @date 2019/1/4 16:53
     * @params  * @param null
     */
    int selectCountUsers();
}