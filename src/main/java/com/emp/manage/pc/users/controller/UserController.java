package com.emp.manage.pc.users.controller;

import com.emp.manage.config.utils.IFollowerResult;
import com.emp.manage.config.utils.PagedResult;
import com.emp.manage.pc.basic.BasicController;
import com.emp.manage.pc.bgm.service.BgmService;
import com.emp.manage.pc.menu.domain.FirstMenu;
import com.emp.manage.pc.menu.service.MenuService;
import com.emp.manage.pc.users.domain.Users;
import com.emp.manage.pc.users.domain.UsersVo;
import com.emp.manage.pc.users.service.UserService;
import com.emp.manage.pc.videos.service.VideoService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.lang3.StringUtils;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/4 10:56
 */
@Controller
@RequestMapping("/users")
public class UserController extends BasicController {
    @Resource
    private UserService userService;
    @Resource
    private BgmService bgmService;
    @Resource
    private VideoService videoService;
    @Resource
    private MenuService menuService;


    @GetMapping("/index")
    public String selecetAllUsers(Model model,Model modelCount){

        int userCount = userService.selectCountUsers();
        int bgmCount = bgmService.selectCountBgm();
        int videoCount = videoService.selectCountVideos();

        modelCount.addAttribute("userCount",userCount);
        modelCount.addAttribute("bgmCount",bgmCount);
        modelCount.addAttribute("videoCount",videoCount);

        return "user/index";

    }


    @GetMapping("/test")
    public String test(){
        return "layout/layout";
    }


    @GetMapping("/list")
    public  String list(Model model,Integer page){
        if (page == null){
            page = 1 ;
        }

        PagedResult result = userService.selectAllUsers(page,PAGE_SIZE);
        int total = result.getTotal();
/*        pageModel.addAttribute("page",total);*/
        model.addAttribute("userList",result);

        return "user/userList";
    }

    /**
     * @author employeeeee
     * @Description: 显示用户详细信息
     * @date 2019/1/7 15:31
     * @params  * @param null
     */
    @GetMapping("/{userid}/queryInfo")
    public  String queryInfo(Model model, HttpSession session, @PathVariable("userid") String userid){
        UsersVo usersVo = userService.queryUsersVoInfo(userid);
        List<FirstMenu> firstMenuList = menuService.selectAllMenu();
        model.addAttribute("userVo", usersVo);
        session.setAttribute("menuList",firstMenuList);
        return "user/userInfo";
    }


    /**
     * @author employeeeee
     * @Description: 删除用户信息
     * @date 2019/1/7 18:26
     * @params  * @param ids 要删除的用户id数组
     */
    @PostMapping("/delete")
    public String delete(Integer[] ids){
        userService.deleteUsersList(ids);
        return "redirect:user/userList";
    }


    /**
     * @author employeeeee
     * @Description: 用户编辑列表界面
     * @date 2019/1/7 18:38
     * @params  * @param page 当前页数
     *                   pageSize 每页显示的数量
     */
    @GetMapping("/eidtList")
    public  String editList(Model model,Integer page,Integer pageSize){
        if (page == null){
            page = 1 ;
        }

        if (pageSize == null){
            pageSize = 1;
        }

        PagedResult result = userService.selectAllUsers(page,pageSize);
        int total = result.getTotal();
        /*        pageModel.addAttribute("page",total);*/
        model.addAttribute("userList",result);

        return "user/userListEdit";
    }

    @RequestMapping("/{userid}/userEdit")
    public String userEdit(Model model,@PathVariable("userid") String userid){
        UsersVo usersVo = userService.queryUsersVoInfo(userid);
        model.addAttribute("userInfo",usersVo);
        return "user/userEdit";
    }


    @PostMapping("/update")
    public String update(Users users){
        String userid = users.getId();
        userService.updateUserInfo(users);
        return "redirect:/users/"+userid+"/userEdit";
    }

    @PostMapping("/imgUploud")
    @ResponseBody
    public IFollowerResult imgUploud(String userId, @RequestParam("file") MultipartFile[] files) throws Exception{
        //文件保存的命令空间
        String fileSpace = "D:/followers-videos";
        //保存到数据库的路径
        String uploadPathDB = "/" + userId + "/face";
        FileOutputStream fileOutputStream = null;
        InputStream inputStream = null;
        if(StringUtils.isBlank(userId)){
            return IFollowerResult.errorMsg("用户ID不能为空");
        }
        try {
            if (files!=null&&files.length>0){

                String fileName = files[0].getOriginalFilename();
                if (StringUtils.isNotBlank(fileName)){
                    //文件上传的最终保存路径
                    String finalFacePath = fileSpace + uploadPathDB + "/" + fileName ;
                    //设置数据库保存路径
                    uploadPathDB += ("/" + fileName);

                    File outFile = new File(finalFacePath);
                    if (outFile.getParentFile()!=null||outFile.getParentFile().isDirectory()){
                        //创建文件夹
                        outFile.getParentFile().mkdirs();
                    }
                    fileOutputStream = new FileOutputStream(outFile);
                    inputStream = files[0].getInputStream();
                    IOUtils.copy(inputStream,fileOutputStream);
                }

            }
            else {
                return IFollowerResult.errorMsg("用户ID不能为空");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return IFollowerResult.errorMsg("用户ID不能为空");
        }finally {
            if (fileOutputStream!=null){
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        }

        return IFollowerResult.ok(uploadPathDB);
    }

    @PostMapping("/saveImage")
    @ResponseBody
    public IFollowerResult saveImage(String userId,HttpServletRequest request, HttpServletResponse response){
        String imgUrl = request.getParameter("imgUrl");
        if (StringUtils.isBlank(imgUrl)){
            return IFollowerResult.ok("noImg");
        }
        Users users = new Users();
        users.setId(userId);
        users.setFaceImage(imgUrl);
        userService.updateUserInfo(users);
        return IFollowerResult.ok("success");

    }




}
