package com.emp.manage.pc.users.dao;

import com.emp.manage.pc.users.domain.UsersVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/7 14:45
 */
@Mapper
@Component
public interface UsersCustomMapper {

   UsersVo queryUsersVoInfo(String userId);


   void deleteUsersList(Integer[] ids);

}
