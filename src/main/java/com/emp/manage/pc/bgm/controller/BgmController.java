package com.emp.manage.pc.bgm.controller;

import com.emp.manage.config.utils.IFollowerResult;
import com.emp.manage.config.utils.MD5Utils;
import com.emp.manage.config.utils.MyUtils;
import com.emp.manage.pc.bgm.domain.Bgm;
import com.emp.manage.pc.bgm.service.BgmService;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/15 10:54
 */
@Controller
@RequestMapping("/bgm")
public class BgmController {


    @Resource
    private BgmService bgmService;

    @GetMapping("/addBgm")
    public String addBgm(Model model){
        model.addAttribute("Bgm",new Bgm());
        return "bgm/insertBgm";
    }


    @PostMapping("/addBgm")
    @ResponseBody
    public IFollowerResult addBgm(@RequestParam("file") MultipartFile[] files){
        //文件保存的命令空间
        String fileSpace = "D:/followers-videos";
        //保存到数据库的路径
        String uploadPathDB = "/bgm";
        FileOutputStream fileOutputStream = null;
        InputStream inputStream = null;
        try {
            if (files!=null&&files.length>0){
                String fileName = files[0].getOriginalFilename();
                if (StringUtils.isNotBlank(fileName)){
                    //文件上传的最终保存路径
                    String finalFacePath = fileSpace + uploadPathDB + "/" + fileName ;
                    //设置数据库保存路径
                    uploadPathDB += ("/" + fileName);

                    File outFile = new File(finalFacePath);
                    if (outFile.getParentFile()!=null||outFile.getParentFile().isDirectory()){
                        //创建文件夹
                        outFile.getParentFile().mkdirs();
                    }
                    fileOutputStream = new FileOutputStream(outFile);
                    inputStream = files[0].getInputStream();
                    IOUtils.copy(inputStream,fileOutputStream);
                }
            }
            else {
                return IFollowerResult.ok("文件路径不能为空");
            }
            if (fileOutputStream!=null){
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return IFollowerResult.ok(uploadPathDB);
    }


    @PostMapping("/uploadbgm")
    public String uploadBgm(Bgm bgm){
        bgmService.SaveBgm(bgm);
        return "redirect:/bgm/addBgm";
    }
}
