package com.emp.manage.pc.bgm.service;

import com.emp.manage.pc.bgm.domain.Bgm;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/4 17:12
 */
public interface BgmService {
    int selectCountBgm();

    void SaveBgm(Bgm bgm);
}
