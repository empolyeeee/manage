package com.emp.manage.pc.bgm.dao;

import com.emp.manage.pc.bgm.domain.Bgm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface BgmMapper {
    int deleteByPrimaryKey(String id);

    int insert(Bgm record);

    int insertSelective(Bgm record);

    Bgm selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Bgm record);

    int updateByPrimaryKey(Bgm record);

    /**
     * @author employeeeee
     * @Description: 查询bgm数量
     * @date 2019/1/4 17:05
     * @params  * @param null
     */
    int selectCountsBgm();
}