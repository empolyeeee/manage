package com.emp.manage.pc.bgm.service;


import com.emp.manage.pc.bgm.dao.BgmMapper;
import com.emp.manage.pc.bgm.domain.Bgm;
import com.org.n3r.idworker.Sid;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/4 17:13
 */
@Service
public class BgmServiceImpl implements BgmService {
    @Resource
    private BgmMapper bgmMapper;

    @Resource
    private Sid sid;

    @Override
    public int selectCountBgm() {

        return bgmMapper.selectCountsBgm();
    }

    @Override
    public void SaveBgm(Bgm bgm) {
        String bgmId = sid.nextShort();
        bgm.setId(bgmId);
        bgmMapper.insertSelective(bgm);
    }
}
