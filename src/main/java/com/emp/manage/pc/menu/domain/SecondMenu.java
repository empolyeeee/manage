package com.emp.manage.pc.menu.domain;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/10 10:12
 */
public class SecondMenu {
    private String id;

    private String menuName;

    private Integer menuValue;

    private String menuUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Integer getMenuValue() {
        return menuValue;
    }

    public void setMenuValue(Integer menuValue) {
        this.menuValue = menuValue;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    @Override
    public String toString() {
        return "SecondMenu{" +
                "id='" + id + '\'' +
                ", menuName='" + menuName + '\'' +
                ", menuValue=" + menuValue +
                ", menuUrl='" + menuUrl + '\'' +
                '}';
    }
}
