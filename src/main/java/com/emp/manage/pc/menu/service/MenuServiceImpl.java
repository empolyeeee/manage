package com.emp.manage.pc.menu.service;

import com.emp.manage.pc.menu.dao.FirstMenuMapper;
import com.emp.manage.pc.menu.domain.FirstMenu;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/10 10:46
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Resource
    private FirstMenuMapper firstMenuMapper;


    @Override
    public List<FirstMenu> selectAllMenu() {
        return firstMenuMapper.selectAllMenu();
    }
}
