package com.emp.manage.pc.menu.domain;

import java.util.List;

public class FirstMenu {
    private String id;

    private String menuImg;

    private String menuName;

    private Integer menuValue;

    private List<SecondMenu> secondMenus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getMenuImg() {
        return menuImg;
    }

    public void setMenuImg(String menuImg) {
        this.menuImg = menuImg == null ? null : menuImg.trim();
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName == null ? null : menuName.trim();
    }

    public Integer getMenuValue() {
        return menuValue;
    }

    public void setMenuValue(Integer menuValue) {
        this.menuValue = menuValue;
    }

    public List<SecondMenu> getSecondMenus() {
        return secondMenus;
    }

    public void setSecondMenus(List<SecondMenu> secondMenus) {
        this.secondMenus = secondMenus;
    }

    @Override
    public String toString() {
        return "FirstMenu{" +
                "id='" + id + '\'' +
                ", menuImg='" + menuImg + '\'' +
                ", menuName='" + menuName + '\'' +
                ", menuValue=" + menuValue +
                ", secondMenus=" + secondMenus +
                '}';
    }
}