package com.emp.manage.pc.menu.service;

import com.emp.manage.pc.menu.domain.FirstMenu;

import java.util.List;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/10 10:45
 */
public interface MenuService {

    /**
     * @author employeeeee
     * @Description: 查询所有菜单
     * @date 2019/1/10 10:45
     * @params  * @param null
     */

    List<FirstMenu> selectAllMenu();
}
