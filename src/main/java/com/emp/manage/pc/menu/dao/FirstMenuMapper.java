package com.emp.manage.pc.menu.dao;


import com.emp.manage.pc.menu.domain.FirstMenu;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface FirstMenuMapper {

    int insert(FirstMenu record);

    int insertSelective(FirstMenu record);

    /**
     * @author employeeeee
     * @Description:  查询所有的一级菜单信息
     * @date 2019/1/9 16:22
     * @params  * @param null
     */
    List<FirstMenu> selectAllMenu();
}