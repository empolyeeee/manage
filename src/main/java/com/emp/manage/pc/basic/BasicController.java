package com.emp.manage.pc.basic;

import org.springframework.stereotype.Controller;

/**
 * @author employeeeee
 * @Descriotion:
 * @date 2019/1/6 10:58
 */
@Controller
public class BasicController {
    /**
     * @author employeeeee
     * @Description: 定义每页展示数量
     * @date 2019/1/6 10:58
     * @params  * @param null
     */
   protected static final Integer PAGE_SIZE = 1;
}
