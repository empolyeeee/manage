package com.emp.manage.pc.reports.dao;


import com.emp.manage.pc.reports.domain.UsersReport;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper

@Component
public interface UsersReportMapper {
    int deleteByPrimaryKey(String id);

    int insert(UsersReport record);

    int insertSelective(UsersReport record);

    UsersReport selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(UsersReport record);

    int updateByPrimaryKey(UsersReport record);
}