package com.emp.manage.config.utils;

import java.util.List;

/**
 * @Description: 封装分页后的数据格式
 */

/**
 * @author employeeeee
 * @Description:
 * page:总页数
 * total:总页数
 * records: 总记录数
 * rows: 每行显示的内容
 * @date 2018/12/18 14:08
 * @params  * @param null
 */
public class PagedResult {


    private int page;

    private int total;

    private long records;

    private List<?> rows;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public long getRecords() {
        return records;
    }

    public void setRecords(long records) {
        this.records = records;
    }

    public List<?> getRows() {
        return rows;
    }

    public void setRows(List<?> rows) {
        this.rows = rows;
    }

}
